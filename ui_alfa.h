/********************************************************************************
** Form generated from reading UI file 'alfa.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ALFA_H
#define UI_ALFA_H

#include <QtCore/QVariant>
#include <QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTreeWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionSaveFile;
    QAction *actionAuthors;
    QAction *actionAboutProgram;
    QAction *actionSaveJson;
    QAction *actionStartServer;
    QWidget *centralwidget;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QFormLayout *formLayout_2;
    QLabel *ip_label;
    QLineEdit *ip_field;
    QLabel *from_label;
    QLineEdit *from_lineEdit;
    QLabel *to_label;
    QLineEdit *to_lineEdit;
    QLabel *profile_label;
    QComboBox *profile_comboBox;
    QProgressBar *progress_scan;
    QDialogButtonBox *scan_buttonBox;
    QTreeWidget *result_TreeWidget;
    QMenuBar *menubar;
    QMenu *manuFile;
    QMenu *menuAuthors;
    QMenu *menuStartServer;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(682, 399);
        actionSaveFile = new QAction(MainWindow);
        actionSaveFile->setObjectName(QString::fromUtf8("actionSaveFile"));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/nowyPrzedrostek/save.png"), QSize(), QIcon::Normal, QIcon::On);
        actionSaveFile->setIcon(icon);
        actionAuthors = new QAction(MainWindow);
        actionAuthors->setObjectName(QString::fromUtf8("actionAuthors"));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/nowyPrzedrostek/authors.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionAuthors->setIcon(icon1);
        actionAboutProgram = new QAction(MainWindow);
        actionAboutProgram->setObjectName(QString::fromUtf8("actionAboutProgram"));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/nowyPrzedrostek/information.png"), QSize(), QIcon::Normal, QIcon::On);
        actionAboutProgram->setIcon(icon2);
        actionSaveJson = new QAction(MainWindow);
        actionSaveJson->setObjectName(QString::fromUtf8("actionSaveJson"));
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/nowyPrzedrostek/save_json.jpg"), QSize(), QIcon::Normal, QIcon::On);
        actionSaveJson->setIcon(icon3);
        actionStartServer = new QAction(MainWindow);
        actionStartServer->setObjectName(QString::fromUtf8("actionStartServer"));
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        verticalLayoutWidget = new QWidget(centralwidget);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(0, 0, 681, 331));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        formLayout_2 = new QFormLayout();
        formLayout_2->setObjectName(QString::fromUtf8("formLayout_2"));
        ip_label = new QLabel(verticalLayoutWidget);
        ip_label->setObjectName(QString::fromUtf8("ip_label"));

        formLayout_2->setWidget(0, QFormLayout::LabelRole, ip_label);

        ip_field = new QLineEdit(verticalLayoutWidget);
        ip_field->setObjectName(QString::fromUtf8("ip_field"));

        formLayout_2->setWidget(0, QFormLayout::FieldRole, ip_field);

        from_label = new QLabel(verticalLayoutWidget);
        from_label->setObjectName(QString::fromUtf8("from_label"));

        formLayout_2->setWidget(1, QFormLayout::LabelRole, from_label);

        from_lineEdit = new QLineEdit(verticalLayoutWidget);
        from_lineEdit->setObjectName(QString::fromUtf8("from_lineEdit"));
        from_lineEdit->setMaxLength(5);

        formLayout_2->setWidget(1, QFormLayout::FieldRole, from_lineEdit);

        to_label = new QLabel(verticalLayoutWidget);
        to_label->setObjectName(QString::fromUtf8("to_label"));

        formLayout_2->setWidget(2, QFormLayout::LabelRole, to_label);

        to_lineEdit = new QLineEdit(verticalLayoutWidget);
        to_lineEdit->setObjectName(QString::fromUtf8("to_lineEdit"));
        to_lineEdit->setMaxLength(5);

        formLayout_2->setWidget(2, QFormLayout::FieldRole, to_lineEdit);

        profile_label = new QLabel(verticalLayoutWidget);
        profile_label->setObjectName(QString::fromUtf8("profile_label"));

        formLayout_2->setWidget(4, QFormLayout::LabelRole, profile_label);

        profile_comboBox = new QComboBox(verticalLayoutWidget);
        profile_comboBox->setObjectName(QString::fromUtf8("profile_comboBox"));

        formLayout_2->setWidget(4, QFormLayout::FieldRole, profile_comboBox);

        progress_scan = new QProgressBar(verticalLayoutWidget);
        progress_scan->setObjectName(QString::fromUtf8("progress_scan"));
        progress_scan->setValue(0);

        formLayout_2->setWidget(5, QFormLayout::LabelRole, progress_scan);

        scan_buttonBox = new QDialogButtonBox(verticalLayoutWidget);
        scan_buttonBox->setObjectName(QString::fromUtf8("scan_buttonBox"));
        scan_buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        formLayout_2->setWidget(5, QFormLayout::FieldRole, scan_buttonBox);


        verticalLayout->addLayout(formLayout_2);

        result_TreeWidget = new QTreeWidget(verticalLayoutWidget);
        QTreeWidgetItem *__qtreewidgetitem = new QTreeWidgetItem();
        __qtreewidgetitem->setText(0, QString::fromUtf8("1"));
        result_TreeWidget->setHeaderItem(__qtreewidgetitem);
        result_TreeWidget->setObjectName(QString::fromUtf8("result_TreeWidget"));

        verticalLayout->addWidget(result_TreeWidget);

        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 682, 29));
        manuFile = new QMenu(menubar);
        manuFile->setObjectName(QString::fromUtf8("manuFile"));
        menuAuthors = new QMenu(menubar);
        menuAuthors->setObjectName(QString::fromUtf8("menuAuthors"));
        MainWindow->setMenuBar(menubar);
        menuStartServer = new QMenu(menubar);
        menuStartServer->setObjectName(QString::fromUtf8("menuStartServer"));
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        MainWindow->setStatusBar(statusbar);

        menubar->addAction(manuFile->menuAction());
        menubar->addAction(menuAuthors->menuAction());
        menubar->addAction(menuStartServer->menuAction());
        menuStartServer->addAction(actionStartServer);
        manuFile->addAction(actionSaveFile);
        manuFile->addAction(actionSaveJson);
        menuAuthors->addAction(actionAuthors);
        menuAuthors->addAction(actionAboutProgram);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "Miko-nsp", nullptr));
        actionSaveFile->setText(QCoreApplication::translate("MainWindow", "Save File", nullptr));
        actionAuthors->setText(QCoreApplication::translate("MainWindow", "Authors", nullptr));
        actionAboutProgram->setText(QCoreApplication::translate("MainWindow", "About program", nullptr));
        actionSaveJson->setText(QCoreApplication::translate("MainWindow", "Save Json", nullptr));
        actionStartServer->setText(QCoreApplication::translate("MainWindow", "StartServer", nullptr));
        ip_label->setText(QCoreApplication::translate("MainWindow", "IP address", nullptr));
        ip_field->setText(QCoreApplication::translate("MainWindow", "127.0.0.1", nullptr));
        from_label->setText(QCoreApplication::translate("MainWindow", "Range from", nullptr));
        from_lineEdit->setText(QCoreApplication::translate("MainWindow", "80", nullptr));
        to_label->setText(QCoreApplication::translate("MainWindow", "Range to ", nullptr));
        to_lineEdit->setText(QCoreApplication::translate("MainWindow", "80", nullptr));
        profile_label->setText(QCoreApplication::translate("MainWindow", "Profile", nullptr));
        manuFile->setTitle(QCoreApplication::translate("MainWindow", "File", nullptr));
        menuAuthors->setTitle(QCoreApplication::translate("MainWindow", "Help", nullptr));
        menuStartServer->setTitle(QCoreApplication::translate("MainWindow", "StartAPI", nullptr));

    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ALFA_H
