#include "progress_counter.hpp"

ProgressCounter::ProgressCounter(int max_progress){
    this->current_progress = 0;
    this->max_progress = max_progress;
}
int ProgressCounter::getValue(){
    return this->current_progress;
}

void ProgressCounter::setValue(int new_value){
    if(new_value!=current_progress){
        current_progress = new_value;
        emit valueChanged(new_value);
    }
}