#ifndef ALFA_H
#define ALFA_H

#include <QtGui>
#include <QMainWindow>
#include <QPushButton>
#include <QFont>
#include <QFileDialog>
#include <QMessageBox>
#include <QTreeWidget>
#include <algorithm>
#include <QtConcurrent>
#include "tcpscan.hpp"
#include <iostream>
#include <QMessageBox>
#include <QTextStream>
#include "synscan.hpp"
#include "finscan.hpp"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    QTreeWidget *hostWidget;

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

//private slots:
  //  void on_pushButton_clicked();

public  slots:
    void on_scan_buttonBox_accepted();
    void on_scan_buttonBox_rejected();
    void updateProgress(const int &value);

private slots:
    void on_actionSaveFile_triggered();
    void on_actionAuthors_triggered();
    void on_actionAboutProgram_triggered();
    void on_actionSaveJson_triggered();
    void on_actionStartServer_triggered();

private:
    Ui::MainWindow *ui;
};

#endif // MainWindow_H
