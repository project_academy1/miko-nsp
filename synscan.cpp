#include "synscan.hpp"


int ip_checksum(u_short *addr,int len){
    int i = len;
    int sum = 0;
    u_short *t=addr;
    u_short result = 0;

    while( i > 1 ){
        sum+=*t++;
        i-=2;
    }
    if(i==1){
        *(u_char*)(&result)=*(u_char*)t;
        sum+=result;
    }
    sum = (sum>>16)+(sum & 0xffff);
    sum += (sum>>16);
    result = ~sum;
    return result;
}


ScannerSYN::ScannerSYN(std::map<std::string,std::vector<Port> > hostIP){
    this->result=hostIP;
    this->progress = new ProgressCounter(hostIP.size());
}

bool ScannerSYN::isPortOpen(std::string ip,int portn){
    struct sockaddr_in addr_s;
    const char* addr = ip.c_str();
    
    short int s=-1;
    
    s = socket(AF_INET,SOCK_RAW,IPPROTO_TCP);
    
    bool res;
    char datagram[4096];
    struct iphdr *iph =(struct iphdr *) datagram;
    struct tcphdr *tcph = (struct tcphdr *) (datagram + sizeof(struct ip));
    char source_ip[20];
    iph->ihl = 5;
    iph->version = 4;
    iph->tos = 0;
    iph->tot_len = sizeof(struct ip) + sizeof(struct tcphdr);
    iph->id = htons(45555);
    iph->frag_off = htons(16384);
    iph->ttl = 64;
    iph->protocol = IPPROTO_TCP;
    iph->saddr = inet_addr(source_ip);
    iph->daddr = inet_addr(addr);
    iph->check = 0;
    int checksum=ip_checksum((u_short*)iph,4*iph->ihl);
    
    iph->check =checksum;
    
    tcph->source = htons(55555);
    tcph->dest = htons(portn);
    tcph->seq = htonl(0);
    tcph->ack_seq = htonl(0);
    tcph->doff = sizeof(struct tcphdr) / 4;
    tcph->fin=0;
    tcph->syn=1;
    tcph->rst=0;
    tcph->psh=0;
    tcph->ack=0;
    tcph->urg=0;
    tcph->window = htons(14600);
    tcph->check = 0;
    tcph->urg_ptr =0;
    int one = 1;
    const int *val =&one;
    if(setsockopt(s,IPPROTO_IP,IP_HDRINCL,val,sizeof(one))<0){
        std::cout<<"ERROR SETTING IP_HDRINCL"<<std::endl;
        close(s);
        shutdown(s,SHUT_RDWR);
        return false;
    }
    if(sendto(s,datagram,sizeof(struct iphdr)+sizeof(struct tcphdr),0,(struct sockaddr*)&addr_s,sizeof(addr_s))<0){
        std::cout<<"ERROR SENDING SYN PACKET"<<std::endl;
        close(s);
        shutdown(s,SHUT_RDWR);
        return false;
    }
    int s2 = socket(AF_INET , SOCK_RAW , IPPROTO_TCP);
    socklen_t saddr_size;
    int data_size;
    struct sockaddr saddr;
    unsigned char *bufor=(unsigned char *)malloc(65536);
    data_size = recvfrom(s2,bufor,65536,0,&saddr, &saddr_size);
    struct iphdr *iphd =(struct iphdr *)bufor;
    struct tcphdr * tcphd =(struct tcphdr*)(bufor+iphd->ihl*4);
    
    if(tcphd->syn==1 && tcphd->ack==1){
        res = true;
    }
    else{
        res = false;
    }
    free(bufor);
    close(s);
    shutdown(s,SHUT_RDWR);
    return res;
}
void ScannerSYN::scanTask(std::string hostIP,std::vector<Port> &hostPorts){
    for(std::vector<Port>::iterator it = hostPorts.begin();it<hostPorts.end();it++){
        bool status=isPortOpen(hostIP,it->getPortNumber());
        it->setPortStatus(status);
        this->progress->setValue(this->progress->getValue()+1);
    }
    
}

std::map<std::string,std::vector<Port> > ScannerSYN::scan(){
    for(std::map<std::string,std::vector<Port> >::iterator it = this->result.begin();it!=this->result.end();it++){
        scanTask(it->first,it->second);
    }
    
    return this->result;
}
