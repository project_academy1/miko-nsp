#include "port.hpp"

Port::Port(unsigned int portNumber){
    this->portNumber=portNumber;
    this->portStatus=false;
};
int Port::getPortNumber(){
    return this->portNumber;
}
bool Port::getPortStatus(){
        return this->portStatus;
}
void Port::setPortStatus(bool portStatus){
        this->portStatus=portStatus;
}
