#include "tcpscan.hpp"

ScannerTCP::ScannerTCP(std::map<std::string,std::vector<Port> > hostIP){
    this->result=hostIP;
    this->progress = new ProgressCounter(hostIP.size());
}

bool ScannerTCP::isPortOpen(std::string ip,int portn){
    struct sockaddr_in addr_s;
    const char* addr = ip.c_str();
    short int s=-1;
    addr_s.sin_family = AF_INET;
    
    addr_s.sin_port = htons(portn);
    inet_pton(addr_s.sin_family,addr,&addr_s.sin_addr);
    s = socket(addr_s.sin_family,SOCK_STREAM,0);
    
    bool result;
    if((connect(s,(struct sockaddr *)&addr_s,sizeof(addr_s))<0)){
        result = false;
    }else{
        result = true;
    }
    close(s);
    shutdown(s,SHUT_RDWR);
    return result;
}
void ScannerTCP::scanTask(std::string hostIP,std::vector<Port> &hostPorts){
    for(std::vector<Port>::iterator it = hostPorts.begin();it<hostPorts.end();it++){
        bool status=isPortOpen(hostIP,it->getPortNumber());
        it->setPortStatus(status);
        
    }
    
}

std::map<std::string,std::vector<Port> > ScannerTCP::scan(){
    
    for(std::map<std::string,std::vector<Port> >::iterator it = this->result.begin();it!=result.end();it++){
        scanTask(it->first,it->second);
    }
    
    return result;
}

 
int test(){
    std::map<std::string,std::vector<Port> > hosts;
    int portBegin=80;
    int portEnd=80;
    std::vector<std::string> hostsIP;
    std::vector<Port> ports;
    hostsIP.push_back("127.0.0.1");
    hostsIP.push_back("192.168.1.1");
    /*
    //alternatywne testowanie
    std::string userInput;
    
    
    std::cout<<"Adresy IP hostow"<<std::endl;
    std::cin>>userInput;
    size_t pos = 0;
    std::string host;
    
    
    std::string delimiter = ",";
    if(userInput.find(delimiter)!= std::string::npos){
        while((pos = userInput.find(delimiter))!= std::string::npos){
            host = userInput.substr(0,pos);
            hostsIP.push_back(host);
            userInput.erase(0,pos+delimiter.length());
        }
    } else{
        hostsIP.push_back(userInput);   
    }
    
    std::cout<<"Poczatek zakresu portow"<<std::endl;
    std::cin>>portBegin;
    std::cout<<"Koniec zakresu portow"<<std::endl;
    std::cin>>portEnd;
    */
    for(int i = portBegin;i<=portEnd;i++){
        Port p(i);
        ports.push_back(p);
    }
    for(std::vector<std::string>::iterator host = hostsIP.begin();host<hostsIP.end();host++){
        std::vector<Port> v(ports);
        hosts[*host] = v;
    }
    ScannerTCP *skaner = new ScannerTCP(hosts);
    std::map<std::string,std::vector<Port> > result = skaner->scan();
    for(std::map<std::string,std::vector<Port> >::iterator it = result.begin();it!=result.end();it++){
        std::cout<<it->first<<":\n\t";
        for(std::vector<Port>::iterator p=it->second.begin();p<it->second.end();p++){
            std::cout<<p->getPortNumber()<<"->";
            if(p->getPortStatus()){
                std::cout<<"open\n\t";
            } else{
                std::cout<<"closed\n\t";
            }
        }
        std::cout<<"\n";
    }
    return 0;
}