FROM debian:stretch-slim as builder
WORKDIR  /project
RUN apt-get update && apt-get install -y g++  qtdeclarative5-dev build-essential qt5-default libcpprest-dev build-essential   && \ 
    apt-get autoremove -y
COPY *.cpp  *.hpp alfa.ui   miko-nsp.pro  ui_alfa.h alfa.h  ./
COPY icons/ ./icons
RUN qmake  miko-nsp.pro  && make

FROM alpine:3.13.6
LABEL description="This is Docker Image for miko-nsp application"
EXPOSE 8887
ENV QT_QPA_PLATFORM=offscreen
ENV QT_QPA_PLATFORM_PLUGIN_PATH=/opt/Qt/${QT_VERSION}/gcc_64/plugins
ENV QT_PLUGIN_PATH=/opt/Qt/${QT_VERSION}/gcc_64/plugins
ENV QT_DEBUG_PLUGINS=1
ENV QT_DEBUG_PLUGINS=1
#ENV QT_QPA_PLATFORM=xcb
ENV DISPLAY=$DISPLAY
VOLUME /tmp/.X11-unix:/tmp/.X11-unix
COPY --from=builder /project/miko-nsp ./
CMD ["./miko-nsp"]
