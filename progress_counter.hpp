#ifndef progress_counter
#define progress_counter
#include <QObject>
class ProgressCounter : public QObject{
    Q_OBJECT
    public:
    ProgressCounter(int max_progress);
    int getValue();

    public slots:
    void setValue(int value);

    signals:
    void valueChanged(int newValue);

    private:
    int current_progress;
    int max_progress;
};

#endif