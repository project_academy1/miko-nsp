#ifndef portMiko
#define portMiko

class Port{
    private:
        int portNumber;
        bool portStatus;
    public:
        Port(unsigned int portNumber);
        int getPortNumber();
        bool getPortStatus();
        void setPortStatus(bool portStatus);
};
#endif