#ifndef tcpscan
#define tcpscan
#include <iostream>
#include <string>
#include <sys/types.h>
#include <sys/socket.h>
#include <vector>
#include <map>
#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <unistd.h>
#include "port.hpp"
#include "progress_counter.hpp"

class ScannerTCP{
    private:
    std::map<std::string,std::vector<Port> > result;
    bool isPortOpen(std::string ip,int portn);
    void scanTask(std::string hostIP,std::vector<Port> &hostPorts);
    public:
    ProgressCounter *progress;
    ScannerTCP(std::map<std::string,std::vector<Port> > hostIP);
    std::map<std::string,std::vector<Port> > scan();
};
#endif