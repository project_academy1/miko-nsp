#include "alfa.h"
#include <vector>
#include <string>
#include <cpprest/http_listener.h>
#include <pplx/pplxtasks.h>
#include <string>
#include <cpprest/http_msg.h>



class BasicController {
    protected:
    web::http::experimental::listener::http_listener _listener;
    private:
    web::uri_builder endpointBuilder;
    public:
    BasicController(const std::string& address,const std::string& port){
        this->endpointBuilder.set_host(address);
        this->endpointBuilder.set_port(port);
        this->endpointBuilder.set_scheme("http");
    }
    
    void setEndpoint(const std::string& mount_point){
        endpointBuilder.set_path(mount_point);
        _listener = web::http::experimental::listener::http_listener(endpointBuilder.to_uri());
    }
    std::string endpoint() const;
    void accept(){
        initRestOpHandlers();
        _listener.open();
    }
    void shutdown(){
        _listener.close();
    }

    virtual void initRestOpHandlers() = 0;

    std::vector<utility::string_t> requestPath(const web::http::http_request & message){
        auto relativePath = web::uri::decode(message.relative_uri().path());
        return web::uri::split_path(relativePath);
    }
    
};

class Service : public BasicController {
    private:
    Ui::MainWindow *ui;
    public:
    Service(const std::string& address,const std::string& port,Ui::MainWindow **ui) : BasicController(address,port) {
        this->ui=*ui;
    }
    
    void handleGet(web::http::http_request message){
        std::vector<std::string> path = requestPath(message);
        if(path.empty()) {
            std::string host = ui->ip_field->text().toStdString();
            std::string from = ui->from_lineEdit->text().toStdString();
            std::string to = ui->to_lineEdit->text().toStdString();
            std::string profile = ui->profile_comboBox->currentText().toStdString();
            if( !(host.empty()) && !(from.empty()) && !(to.empty()) ){ 
                web::json::value res = scan(host,from,to,profile);
                std::cout<<res<<std::endl;
                message.reply(web::http::status_codes::OK,res);
            }  
        }else if(path[0]=="scan"){
            if((!path[1].empty())&&(!path[2].empty())&&(!path[3].empty())&&(!path[4].empty())){
                std::string host = path[1];
                std::string from = path[2];
                std::string to = path[3];
                std::string profile;
                if(path[4]=="SYN"){
                    profile="TCP SYN";
                }else if(path[4]=="FIN"){
                    profile="TCP FIN";
                }else if(path[4]=="CON"){
                    profile="TCP CONNECT";
                }else{
                    profile="TCP CONNECT";
                };
                if( !(host.empty()) && !(from.empty()) && !(to.empty())){ 
                    
                    web::json::value res = scan(host,from,to,profile);
                    std::cout<<res<<std::endl;
                    message.reply(web::http::status_codes::OK,res);
                }else{
                    message.reply(web::http::status_codes::BadRequest);
                }
            }
            else{
                message.reply(web::http::status_codes::BadRequest);
            }

        }
        else {
            message.reply(web::http::status_codes::BadRequest);
        }
    }
    void handlePost(web::http::http_request message){
    message.extract_json().then([=](pplx::task<web::json::value> task)
    {
        try
        {
            web::json::value val = task.get();
            std::string ip = val[U("ip")].as_string();
            int port_start = val[U("pstart")].as_number().to_int32();
            int port_end = val[U("pend")].as_number().to_int32();
            web::json::value res=scan(ip,std::to_string(port_start),std::to_string(port_end),"TCP CONNECT");
            message.reply(web::http::status_codes::OK,res);
            
        }
        catch(std::exception& e) {
            message.reply(web::http::status_codes::BadRequest);
            std::cout<<e.what()<<std::endl;
        }
    });
    }
    web::json::value scan(std::string host,std::string from, std::string to, std::string profile){
        std::map<std::string,std::vector<Port> > hosts;
        int portBegin=stoi(from);
        int portEnd=stoi(to);
        std::vector<std::string> hostsIP;
        std::vector<Port> ports;
        std::string input=host;
        std::string::iterator end_pos = std::remove(input.begin(),input.end(), ' ');
        input.erase(end_pos,input.end());
        size_t pos = 0;
        std::string addr;
        std::string delimiter = ",";
        if(input.find(delimiter)!= std::string::npos){
            while((pos = input.find(delimiter))!= std::string::npos){
                addr = input.substr(0,pos);
                hostsIP.push_back(addr);
                input.erase(0,pos+delimiter.length());
            }
            hostsIP.push_back(input);
        } 
        else{
            hostsIP.push_back(input);   
        }
        for(int i = portBegin;i<=portEnd;i++){
            Port p(i);
            ports.push_back(p);
        }
        for(std::vector<std::string>::iterator host = hostsIP.begin();host<hostsIP.end();host++){
            std::vector<Port> v(ports);
            hosts[*host] = v;
        }
        std::map<std::string,std::vector<Port> > result;
        if(profile =="TCP SYN"){
            ScannerSYN *skan = new ScannerSYN(hosts);
            result = skan->scan();
        }
        else if(profile =="TCP FIN"){
            ScannerFIN *skan = new ScannerFIN(hosts);
            result = skan->scan();
        }
        else{
            ScannerTCP *skan = new ScannerTCP(hosts);
            result = skan->scan();
        }     
        web::json::value res = web::json::value::object();
        for(std::map<std::string,std::vector<Port> >::iterator it = result.begin();it!=result.end();it++){
            web::json::value obj = web::json::value::object();
            for(std::vector<Port>::iterator p=it->second.begin();p<it->second.end();p++){
                web::json::value item = web::json::value::object();
                obj[U(std::to_string(p->getPortNumber()))] = web::json::value(p->getPortStatus());
                
            }
            res[U(it->first)]=obj;
        }
        return res;
    }
    void initRestOpHandlers() override{
    _listener.support(web::http::methods::GET,std::bind(&Service::handleGet,this,std::placeholders::_1));
    _listener.support(web::http::methods::POST,std::bind(&Service::handlePost,this,std::placeholders::_1));
}
    
    
    
};


