#include "alfa.h"
#include "ui_alfa.h"
#include "server.cpp"
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QStringList profiles = QStringList()<<tr("TCP connect")<<tr("TCP SYN")<<tr("TCP FIN");
    ui->profile_comboBox->insertItems(0,profiles);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_scan_buttonBox_accepted(){
    QString host = ui->ip_field->text();
    QString from = ui->from_lineEdit->text();
    QString to = ui->to_lineEdit->text();
    QString profile = ui->profile_comboBox->currentText();
  
    if (host.toStdString().empty()){
       QMessageBox::critical(this, "Field ip_field", "The value of the 'IP address' field is empty");
    }
    
     if (from.toStdString().empty()){
        QMessageBox::critical(this, "Field from_lineEdit", "The value of the 'Range from' field is empty");
    }

     if (to.toStdString().empty()){
        QMessageBox::critical(this, "Field to_lineEdit", "The value of the 'Range to' field is empty");
    }
    if( !(host.toStdString().empty()) && !(from.toStdString().empty()) && !(to.toStdString().empty()) ){ 
        std::map<std::string,std::vector<Port> > hosts;
        int portBegin=stoi(from.toStdString());
        int portEnd=stoi(to.toStdString());
        std::vector<std::string> hostsIP;
        std::vector<Port> ports;
        std::string input=host.toStdString();
        std::string::iterator end_pos = std::remove(input.begin(),input.end(), ' ');
        input.erase(end_pos,input.end());
        size_t pos = 0;
        std::string addr;
        
        std::string delimiter = ",";
        if(input.find(delimiter)!= std::string::npos){
            while((pos = input.find(delimiter))!= std::string::npos){
                addr = input.substr(0,pos);
                hostsIP.push_back(addr);
                input.erase(0,pos+delimiter.length());
            
            }
            hostsIP.push_back(input);
        } else{
            hostsIP.push_back(input);   
        }
        for(int i = portBegin;i<=portEnd;i++){
            Port p(i);
            ports.push_back(p);
        }
        for(std::vector<std::string>::iterator host = hostsIP.begin();host<hostsIP.end();host++){
            std::vector<Port> v(ports);
            hosts[*host] = v;
        }
        if(profile == "TCP connect"){
            QMessageBox::information(this, "Scan", "Running a " +  profile + " type scan");
            QFuture<void> task = QtConcurrent::run([=](){
                ScannerTCP *skan = new ScannerTCP(hosts);
                ui->progress_scan->setRange(0,hosts.size());
                QObject::connect(skan->progress,&ProgressCounter::valueChanged,
                                 ui->progress_scan, &QProgressBar::setValue);
                std::map<std::string,std::vector<Port> > result = skan->scan();
                ui->result_TreeWidget->clear();
                for(std::map<std::string,std::vector<Port> >::iterator it = result.begin();it!=result.end();it++){
                    QTreeWidgetItem *hostIP = new QTreeWidgetItem(ui->result_TreeWidget);
                    hostIP->setText(0,tr(it->first.c_str()));
                    std::cout<<it->first<<":\n\t";
                    for(std::vector<Port>::iterator p=it->second.begin();p<it->second.end();p++){
                        QTreeWidgetItem *portItem = new QTreeWidgetItem(hostIP);
                        portItem->setText(0,tr(std::to_string(p->getPortNumber()).c_str()));
                        std::cout<<p->getPortNumber()<<"->";
                        QTreeWidgetItem *portStatusItem = new QTreeWidgetItem(portItem);
                        if(p->getPortStatus()){
                            portStatusItem->setText(0,tr("open"));
                            std::cout<<"open\n\t";
                        } else{
                            portStatusItem->setText(0,tr("closed"));
                            std::cout<<"closed\n\t";
                        }
                    }
                    std::cout<<"\n";
                }
                QMessageBox::information(this, "Scan", "Scan is complete");
                ui->progress_scan->reset();
            });   
        }
        else if(profile == "TCP SYN"){
            QMessageBox::information(this, "Scan", "Running a " +  profile + " type scan");
            QFuture<void> task = QtConcurrent::run([=](){
                auto skan = new ScannerSYN(hosts);
                ui->progress_scan->setRange(0,hosts.size());
                QObject::connect(skan->progress,&ProgressCounter::valueChanged,
                                 ui->progress_scan, &QProgressBar::setValue);
                std::map<std::string,std::vector<Port> > result = skan->scan();
                ui->result_TreeWidget->clear();
                for(std::map<std::string,std::vector<Port> >::iterator it = result.begin();it!=result.end();it++){
                    QTreeWidgetItem *hostIP = new QTreeWidgetItem(ui->result_TreeWidget);
                    hostIP->setText(0,tr(it->first.c_str()));
                    std::cout<<it->first<<":\n\t";
                    for(std::vector<Port>::iterator p=it->second.begin();p<it->second.end();p++){
                        QTreeWidgetItem *portItem = new QTreeWidgetItem(hostIP);
                        portItem->setText(0,tr(std::to_string(p->getPortNumber()).c_str()));
                        std::cout<<p->getPortNumber()<<"->";
                        QTreeWidgetItem *portStatusItem = new QTreeWidgetItem(portItem);
                        if(p->getPortStatus()){
                            portStatusItem->setText(0,tr("open"));
                            std::cout<<"open\n\t";
                        } else{
                            portStatusItem->setText(0,tr("closed"));
                            std::cout<<"closed\n\t";
                        }
                    }
                    std::cout<<"\n";    
                }
                QMessageBox::information(this, "Scan", "Scan is complete");
                ui->progress_scan->reset();
            });
        }
        else if(profile == "TCP FIN"){
            QMessageBox::information(this, "Scan", "Running a " +  profile + " type scan");
            QFuture<void> task = QtConcurrent::run([=](){
                auto skan = new ScannerFIN(hosts);
                ui->progress_scan->setRange(0,hosts.size());
                QObject::connect(skan->progress,&ProgressCounter::valueChanged,
                                 ui->progress_scan, &QProgressBar::setValue);
                std::map<std::string,std::vector<Port> > result = skan->scan();
                ui->result_TreeWidget->clear();
                
                for(std::map<std::string,std::vector<Port> >::iterator it = result.begin();it!=result.end();it++){
                    QTreeWidgetItem *hostIP = new QTreeWidgetItem(ui->result_TreeWidget);
                    hostIP->setText(0,tr(it->first.c_str()));
                    std::cout<<it->first<<":\n\t";
                    for(std::vector<Port>::iterator p=it->second.begin();p<it->second.end();p++){
                        QTreeWidgetItem *portItem = new QTreeWidgetItem(hostIP);
                        portItem->setText(0,tr(std::to_string(p->getPortNumber()).c_str()));
                        std::cout<<p->getPortNumber()<<"->";
                        QTreeWidgetItem *portStatusItem = new QTreeWidgetItem(portItem);
                        if(p->getPortStatus()){
                            portStatusItem->setText(0,tr("open"));
                            std::cout<<"open\n\t";
                        } else{
                            portStatusItem->setText(0,tr("closed"));
                            std::cout<<"closed\n\t";
                        }
                    }
                    std::cout<<"\n";            
                }
                QMessageBox::information(this, "Scan", "Scan is complete");
                ui->progress_scan->reset();
            });
        }
    }
}

void MainWindow::on_scan_buttonBox_rejected(){
    QMessageBox::information(this, "Cancel", "Scans have been cancelled");
}

void MainWindow::updateProgress(const int &value){
    ui->progress_scan->setValue(value);
}

void MainWindow::on_actionSaveFile_triggered()
{   
    QString file_name= QFileDialog::getSaveFileName(this,tr("Save File"), QDir::homePath(), tr("*.scan"));
    file_name = file_name + ".scan";
    QFile file(file_name);
    QString sResult = "";
    for(int i = 0; i <  ui->result_TreeWidget->topLevelItemCount(); i++)
    {
        for (int j = 0; j <  ui->result_TreeWidget->columnCount(); j++) //fix: j++
        {
            sResult +=  ui->result_TreeWidget->topLevelItem(i)->text(j) + ":"+"\t\n"; 
            for (int x=0; x < ui->result_TreeWidget->topLevelItem(i)->childCount(); x++){
                sResult += "\t" + ui->result_TreeWidget->topLevelItem(i)->child(x)->text(j) +  "->";
                for (int z=0; z < ui->result_TreeWidget->topLevelItem(i)->child(x)->childCount(); z++){
                    sResult += ui->result_TreeWidget->topLevelItem(i)->child(x)->child(z)->text(j) + "\t\n";
                }
            }
        }
        sResult += "\n";  
        std::string str =  sResult.toStdString();
        std::cout << str << std::endl;
    }
    if (!file.open(QIODevice::WriteOnly)){
        QMessageBox::critical(this, "Save file", "Failed to create " + file_name + " file");
    }
    QTextStream out(&file);
    out << sResult;
    file.close();
    if(file_name != ".scan"){
        QMessageBox::information(this, "Save file", "The " + file_name + "file was successfully created");
    }
}

void MainWindow::on_actionSaveJson_triggered(){
    QString file_name= QFileDialog::getSaveFileName(this,tr("Save File"), QDir::homePath(), tr("*.json"));
    file_name = file_name + ".json";
    QFile file(file_name);
    QString sResult = "{\n \"hosts\":{ \n";
    
    for(int i = 0; i <  ui->result_TreeWidget->topLevelItemCount(); i++)
    {
        for (int j = 0; j <  ui->result_TreeWidget->columnCount(); j++) //fix: j++
        {
            sResult +=  "\t\t\"" + ui->result_TreeWidget->topLevelItem(i)->text(j) + "\":{"+"\t\n"; 
            for (int x=0; x < ui->result_TreeWidget->topLevelItem(i)->childCount(); x++){
                sResult += "\t\t\t\"" + ui->result_TreeWidget->topLevelItem(i)->child(x)->text(j) +  "\":{";
                for (int z=0; z < ui->result_TreeWidget->topLevelItem(i)->child(x)->childCount(); z++){
                    if (x ==ui->result_TreeWidget->topLevelItem(i)->childCount()-1){
                        sResult += "\"status\": \"" +ui->result_TreeWidget->topLevelItem(i)->child(x)->child(z)->text(j) + "\"}\t\n";
                    }
                    else {
                        sResult += "\"status\": \"" +ui->result_TreeWidget->topLevelItem(i)->child(x)->child(z)->text(j) + "\"},\t\n";
                    }
                }
            }
            if (i == ui->result_TreeWidget->topLevelItemCount()-1){
                sResult += "\t\t}\n";
            }
            else {
                sResult += "\t\t},\n";
            }
        }
    }
    sResult += "  }\n";  
    sResult += "}\n";  
    std::string str =  sResult.toStdString();
    std::cout << str << std::endl;
    if (!file.open(QIODevice::WriteOnly)){
        QMessageBox::critical(this, "Save file", "Failed to create " + file_name + " file");
    }
    QTextStream out(&file);
    out << sResult;
    file.close();
    if(file_name != ".scan"){
        QMessageBox::information(this, "Save file", "The " + file_name + "file was successfully created");
    }
}


void MainWindow::on_actionAuthors_triggered()
{
    QWidget *Window= new QWidget();
    QLabel *name_label_4 = new QLabel(Window);; 
    QLabel *name_label_3 = new QLabel(Window);
    QLabel *name_label = new QLabel(Window);;
    QLabel *name_label_2 = new QLabel(Window);
    QFont font;
    name_label_4->setObjectName(QString::fromUtf8("name_label_4"));
    name_label_4->setGeometry(QRect(60, 30, 261, 39));
    font.setFamily(QString::fromUtf8("Umpush"));
    font.setBold(true);
    font.setItalic(false);
    font.setWeight(QFont::Weight(75));
    name_label_4->setFont(font);
    name_label_3->setObjectName(QString::fromUtf8("name_label_3"));
    name_label_3->setGeometry(QRect(10, 30, 51, 39));
    name_label_3->setFont(font);
    name_label->setObjectName(QString::fromUtf8("name_label"));
    name_label->setGeometry(QRect(60, 0, 271, 39));
    name_label->setFont(font); 
    name_label_2->setObjectName(QString::fromUtf8("name_label_2"));
    name_label_2->setGeometry(QRect(10, 0, 51, 39));
    name_label_2->setFont(font);
    name_label_4->setText(QCoreApplication::translate("Window", "Hagaven(https://gitlab.com/hagaven)", nullptr));
    name_label_3->setText(QCoreApplication::translate("Window", "Author:", nullptr));
    name_label->setText(QCoreApplication::translate("Window", "Goodbit22(https://gitlab.com/goodbit22)", nullptr));
    name_label_2->setText(QCoreApplication::translate("Window", "Author:", nullptr));
    Window->setObjectName(QString::fromUtf8("Window"));
    Window->resize(328, 77);    
    Window->setWindowTitle(QCoreApplication::translate("Window", "Authors", nullptr)); 
    Window->show();
}

void MainWindow::on_actionAboutProgram_triggered()
{
    QWidget *Window= new QWidget();
    QLabel *name_label = new QLabel(Window);
    QFont font;
    QPushButton *ok = new QPushButton(Window);
    QLabel *version_label = new QLabel(Window);
    name_label->setObjectName(QString::fromUtf8("name_label"));
    name_label->setGeometry(QRect(120, 10, 81, 17));
    name_label->setText(QCoreApplication::translate("Window", "Miko-nsp", nullptr));
    font.setFamily(QString::fromUtf8("Umpush"));
    font.setBold(true);
    font.setItalic(false);
    font.setWeight(QFont::Weight(75));
    name_label->setFont(font);
    ok->setObjectName(QString::fromUtf8("ok"));
    ok->setGeometry(QRect(120, 70, 61, 33));
    ok->setText(QCoreApplication::translate("Window", "OK", nullptr));
    version_label->setObjectName(QString::fromUtf8("version_label"));
    version_label->setGeometry(QRect(10, 40, 91, 17));
    version_label->setText(QCoreApplication::translate("Window", "Version: 1.1", nullptr));
    Window->setObjectName(QString::fromUtf8("Window"));
    Window->setWindowTitle(QCoreApplication::translate("Window", "About Program", nullptr));
    Window->resize(292, 109);
    Window->show();
}
void MainWindow::on_actionStartServer_triggered(){
    QFuture<void> task = QtConcurrent::run([=](){
    std::cout << "Api zaczyna prace" <<std::endl;
        Service serv("127.0.0.1","8080",&ui);
        serv.setEndpoint("/api");
        serv.accept();
        while(1==1)
        {
            sleep(1000);
        };});
        
}




