# miko nsp

 A simple graphical port scanner to scan types such as SYN, FIN and TCP connect using C++ and raw sockets.

![graphic1](./img/graphic.png)

![graphic2](./img/graphic2.png)

## Table of Contents

- [miko nsp](#miko-nsp)
  - [Table of Contents](#table-of-contents)
  - [Features](#features)
  - [Class Diagram](#class-diagram)
  - [Running and installation](#running-and-installation)
    - [Compilation](#compilation)
    - [Running](#running)
  - [Technologies](#technologies)
  - [Authors](#authors)
  - [License](#license)

## Features

- Type scans (Syn, UDP, FIN)
- Ability to scan using the REST API
- Save scan results to txt and json files
- GUI

## Class Diagram

![class](./img/Class_Diagram.png)

## Running and installation

### Compilation

To compile the project you just need to type commands in the terminal

```sh
qmake miko-nsp.pro
make
```

### Running

```sh
sudo ./miko-nsp
```

## Technologies

Project is created with:

- C++
- Qt5
- STL
- Socket
- Cablanca cpprest

## Authors

*****
__goodbit22__ --> 'https://gitlab.com/users/goodbit22'
*****
__hagaven__   --> 'https://gitlab.com/hagaven'

## License

   Apache License Version 2.0
